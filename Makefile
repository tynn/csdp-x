#	This file is part of csdp-x.
#
#	Copyright (c) 2014 Christian Schmitz <tynn.dev@gmail.com>
#
#	csdp-x is free software: you can redistribute it and/or modify
#	it under the terms of the GNU General Public License as published by
#	the Free Software Foundation, either version 3 of the License, or
#	(at your option) any later version.
#
#	csdp-x is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with csdp-x. If not, see <http://www.gnu.org/licenses/>.

export CC = gcc

CSDP = Csdp-6.1.1
LIBDIR = $(CURDIR)/csdp
export CSDPLIB = $(LIBDIR)/$(CSDP)
LIBSDP = .

CFLAGS = -O3 -Wall -g
CCFLAGS = -imacros"$(CURDIR)/bit64.h" -DNOSHORTS -DUSEGETTIME -I"$(CSDPLIB)/include"

export LDLIBS = -llapack -lblas -lm

ifneq (,$(or $(findstring OPENMP,$(WITH)),$(findstring openmp,$(WITH))))
CCFLAGS += -fopenmp -DUSEOPENMP -DSETNUMTHREADS
LDLIBS += -lgomp -lrt -lpthread
endif

export MAKELIB = $(MAKE) -C "$(CURDIR)" csdplib CFLAGS='$(CFLAGS) $(CCFLAGS)' CCFLAGS=''


export prefix = /usr/local
export exec_prefix = $(prefix)
export includedir = $(prefix)/include
export bindir = $(exec_prefix)/bin
export libdir = $(exec_prefix)/lib

export INSTALL = install
export INSTALL_PROGRAM = $(INSTALL)
export INSTALL_DATA = $(INSTALL) -m 644


export LIBCSDP = $(CURDIR)/libcsdp

MAKEDIRS = libcsdp csdpval csdp-ld python-csdp
INSTALLDIRS = $(MAKEDIRS:%=install-%)
CLEANDIRS = $(MAKEDIRS:%=clean-%)

.PHONY: all $(MAKEDIRS) csdplib $(CSDPLIB) install installdirs $(INSTALLDIRS) clean $(CLEANDIRS) distclean


all: $(MAKEDIRS)

path:
	@echo 'export LD_LIBRARY_PATH="$(CURDIR)/libcsdp:$$LD_LIBRARY_PATH"' > path
	@echo 'export PYTHONPATH="$(CURDIR)/python-csdp:$$PYTHONPATH"' >> path
	@echo 'export PATH="$(CURDIR)/csdp-ld:$$PATH"' >> path

$(MAKEDIRS):
	@$(MAKE) -C $@ CFLAGS='$(CFLAGS) $(CCFLAGS)'

csdplib: $(CSDPLIB)
	$(eval CSDP_LIB = $(LIBDIR)/lib$(subst $(eval) ,,$(sort $(CCFLAGS))))
	rsync -rtu --exclude '*.o' "$</lib/" "$(CSDP_LIB)"
	@$(MAKE) -C "$(CSDP_LIB)" CFLAGS='$(CFLAGS) $(CCFLAGS)' LDLIBS='$(LDLIBS)'
	cp "$(CSDP_LIB)/libsdp.a" "$(LIBSDP)"

$(CSDPLIB): $(CSDPLIB).tgz
	@echo 8388e8988e337bb5c1291068828de801 $< | md5sum -c
	tar -C "$(LIBDIR)" -xzf "$<"

$(CSDPLIB).tgz:
	mkdir -p "$(LIBDIR)"
	@wget "http://www.coin-or.org/download/source/Csdp/$(CSDP).tgz" -O "$@"

install: installdirs $(INSTALLDIRS)

installdirs:
	$(INSTALL) -d "$(DESTDIR)$(prefix)" "$(DESTDIR)$(exec_prefix)" "$(DESTDIR)$(includedir)" "$(DESTDIR)$(bindir)" "$(DESTDIR)$(libdir)"

$(INSTALLDIRS):
	@$(MAKE) -C $(@:install-%=%) install CFLAGS='$(CFLAGS) $(CCFLAGS)' DESTDIR='$(realpath $(DESTDIR))'

clean: mostlyclean
	$(foreach lib,$(wildcard $(LIBDIR)/lib*) $(CSDPLIB),$(MAKE) -C "$(lib)" clean;)

$(CLEANDIRS):
	@$(MAKE) -C $(@:clean-%=%) clean

distclean: clean
	$(RM) -r "$(LIBDIR)"

mostlyclean: $(CLEANDIRS)
	$(RM) path

maintainer-clean: distclean

