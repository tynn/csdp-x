#	This file is part of csdp-x.
#
#	Copyright (c) 2014 Christian Schmitz <tynn.dev@gmail.com>
#
#	csdp-x is free software: you can redistribute it and/or modify
#	it under the terms of the GNU General Public License as published by
#	the Free Software Foundation, either version 3 of the License, or
#	(at your option) any later version.
#
#	csdp-x is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with csdp-x. If not, see <http://www.gnu.org/licenses/>.

ijtok = lambda iiii, jjjj, lda : (jjjj - 1) * lda + iiii - 1
ijtokp = lambda iii, jjj, lda : iii + jjj * (jjj - 1) // 2 - 1
ktoi = lambda k, lda : k % lda + 1
ktoj = lambda k, lda : k // lda + 1

