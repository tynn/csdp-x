#	This file is part of csdp-x.
#
#	Copyright (c) 2014 Christian Schmitz <tynn.dev@gmail.com>
#
#	csdp-x is free software: you can redistribute it and/or modify
#	it under the terms of the GNU General Public License as published by
#	the Free Software Foundation, either version 3 of the License, or
#	(at your option) any later version.
#
#	csdp-x is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with csdp-x. If not, see <http://www.gnu.org/licenses/>.

from . import declarations as csdp
import numpy, operator, scipy.linalg, scipy.sparse, tempfile

def _blocks (A) :
	if scipy.sparse.issparse(A) :
		d = A - A.T
		assert numpy.allclose(d.data, numpy.zeros(d.nnz)), "matrix not symmetrical"
		A = A.todok()
	else :
		assert numpy.allclose(A, A.T), "matrix not symmetrical"
		A = numpy.asarray(A)
	n, blocks = A.shape[0], []
	while A.shape != (0, 0) :
		rs, bs = 0, 1
		while bs < n :
			nz = list(zip(*numpy.nonzero(A[rs:bs, bs:])))
			if not len(nz) :
				break
			rs = bs
			bs += max(nz, key = operator.itemgetter(1))[1] + 1
		blocks.append(bs)
		A = A[bs:,bs:]
	return blocks

def _extend_block (A, bc, bs, d = 1) :
	for b in A :
		B = 0
		while B < bs :
			B += b.pop(bc)
		b.insert(bc, d * B)
		if B > bs :
			return B
	return bs

def _entry (A, mn, bs) :
	A = A.todok() if scipy.sparse.issparse(A) else numpy.asarray(A)
	b0 = 0
	for bn, b1 in enumerate(bs) :
		bn += 1
		b1 = b0 + abs(b1)
		B = A[b0:b1, b0:b1]
		nz = filter(lambda i : i[0] <= i[1], zip(*numpy.nonzero(B)))
		for e in map(lambda i : (i[0] + 1, i[1] + 1, B[i]), nz) :
			yield "\n{} {} {} {} {:e}".format(mn, bn, *e)
		b0 = b1


def find_blocks (*A) :
	A = list(map(_blocks, A))
	bc = map(sum, A)
	B = max(bc)
	for i, v in enumerate(bc) :
		A[i].extend([1] * (B - v))
	A = list(map(list, set(map(tuple, A))))
	bc = 0
	while len(A) > 1 :
		bd, bs = bc, 1
		while bs == 1 :
			bs = max(list(zip(*A))[bd])
			bd += 1
		bd -= bc + 1
		if bd :
			_extend_block(A, bc, bd, -1)
			bc += 1
		bd, bs = bs, 0
		while bd != bs :
			bs = bd
			bd = _extend_block(A, bc, bs)
		A = list(map(list, set(map(tuple, A))))
		bc += 1
	B, A = A, A[0]
	l = len(A)
	while bc < l :
		bd, bs = bc, 1
		while bd < l and A[bd] == 1 :
			bd += 1
		bd -= bc
		if bd :
			_extend_block(B, bc, bd, -1)
			l -= bd + 1
		bc += 1
	return A


def write_prob (f, C, a, constraints, blocks = None) :
	assert len(a) >= len(constraints), "too many constraints"
	C = [C] + constraints
	bs = blocks or find_blocks(*C)
	f.write("{}\n{}\n".format(len(a), len(bs)))
	f.write(" ".join(map(str, bs)))
	f.write("\n")
	f.write(" ".join(map(lambda v : format(v, 'e'), a)))
	for i, A in enumerate(C) :
		for line in _entry(A, i, bs) :
			f.write(line);
	f.flush()


def load_prob (C, a, constraints, blocks = None) :
	with tempfile.NamedTemporaryFile('w') as f :
		write_prob(f, C, a, constraints, blocks)
		n, k, C = csdp.c_int(), csdp.c_int(), csdp.blockmatrix()
		a, constraints = csdp.c_double_p(), csdp.constraintmatrix_p()
		if csdp.read_prob(f.name, n, k, C, a, constraints, 1) :
			raise RuntimeError
	X, y, Z = csdp.blockmatrix(), csdp.c_double_p(), csdp.blockmatrix()
	csdp.initsoln(n, k, C, a, constraints, X, y, Z)
	return n, k, C, a, constraints, X, y, Z


def blockmatrix (*blocks) :
	""" assemble a numpy matrix from given blocks diagonally """
	return scipy.linalg.block_diag(*blocks)


def block (A, i = None) :
	if isinstance(A, csdp.blockmatrix) :
		if not i or A.nblocks < i or i < 0  :
			raise ValueError("must be 1 <= i <= {}".format(A.nblocks))
		A = A.blocks[i]
	elif not isinstance(A, csdp.blockrec) :
		raise TypeError(
					"must be a csdp block or matrix type, not {}"
						.format(type(A)))
	if A.blockcategory == csdp.blockcat.DIAG :
		return numpy.diag(A.data.vec[1:A.blocksize+1])
	n = A.blocksize
	return numpy.matrix(A.data.mat[0:n*n]).reshape((n, n))


def matrix (A) :
	""" create a numpy matrix from a CSDP blockmatrix """
	if not isinstance(A, csdp.blockmatrix) :
		raise TypeError("must be a csdp.blockmatrix, not {}".format(type(A)))
	blocks = []
	for i in range(1, A.nblocks + 1) :
		blocks.append(block(A.blocks[i]))
	return blockmatrix(*blocks)


def solve_prob (C, a, constraints, blocks = None) :
	""" solve a CSDP problem"""
	n, k, C, a, constraints, X, y, Z = load_prob(C, a, constraints, blocks)
	pobj, dobj = csdp.c_double(0.0), csdp.c_double(0.0)
	ret = csdp.easy_sdp(n, k, C, a, constraints, 0.0, X, y, Z, pobj, dobj)
	ret = (ret, matrix(X), y[1:k.value+1], matrix(Z))
	csdp.free_prob(n, k, C, a, constraints, X, y, Z)
	return ret


class SDP (object) :
	_attr = ('n', 'k', 'C', 'a', 'constraints', 'X', 'y', 'Z')

	def __init__ (self, C, a, constraints) :
		self._prob = load_prob(C, a, constraints)
		self._obj = csdp.c_double(0.0), csdp.c_double(0.0)

	def __del__ (self) :
		if hasattr(self, '_prob') :
			csdp.free_prob(*self._prob)

	def __setattr__ (self, name, value) :
		if name == 'a' :
			k = self.k
			assert k <= len(value), "value must have a length of " + str(k)
			self._prob[3][1:k+1] = value[:k]
		else :
			object.__setattr__(self, name, value)

	def __getattr__ (self, name) :
		try :
			value = self._prob[self._attr.index(name)]
			if isinstance(value, csdp.blockmatrix) :
				return matrix(value)
			elif isinstance(value, csdp.c_double_p) :
				return value[1:self.k+1]
			elif isinstance(value, csdp.c_int) :
				return value.value
			else :
				return value # TODO constraints
		except :
			return object.__getattr__(self, name)

	def solve (self) :
		args = self._prob[:5] + (0.0,) + self._prob[5:] + self._obj
		return csdp.easy_sdp(*args)

