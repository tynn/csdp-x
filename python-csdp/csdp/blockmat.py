#	This file is part of csdp-x.
#
#	Copyright (c) 2014 Christian Schmitz <tynn.dev@gmail.com>
#
#	csdp-x is free software: you can redistribute it and/or modify
#	it under the terms of the GNU General Public License as published by
#	the Free Software Foundation, either version 3 of the License, or
#	(at your option) any later version.
#
#	csdp-x is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with csdp-x. If not, see <http://www.gnu.org/licenses/>.

from ctypes import c_double, c_int, POINTER, Structure, Union

c_double_p = POINTER(c_double)
c_int_p = POINTER(c_int)

class blockcat (object) :
	DIAG = 0
	MATRIX = 1
	PACKEDMATRIX = 2

class blockdatarec (Union) :
	_fields_ = [
		("vec", c_double_p),
		("mat", c_double_p),
	]

class blockrec (Structure) :
	_fields_ = [
		("data", blockdatarec),
		("blockcategory", c_int),
		("blocksize", c_int),
	]
blockrec_p = POINTER(blockrec)

class blockmatrix (Structure) :
	_fields_ = [
		("nblocks", c_int),
		("blocks", blockrec_p),
	]

class sparseblock (Structure) : pass
sparseblock_p = POINTER(sparseblock)
sparseblock._fields_ = [
	("next", sparseblock_p),
	("nextbyblock", sparseblock_p),
	("entries", c_double_p),
	("iindices", c_int_p),
	("jindices", c_int_p),
	("numentries", c_int),
	("blocknum", c_int),
	("blocksize", c_int),
	("constraintnum", c_int),
	("issparse", c_int),
]

class constraintmatrix (Structure) :
	_fields_ = [("blocks", sparseblock_p)]
constraintmatrix_p = POINTER(constraintmatrix)

