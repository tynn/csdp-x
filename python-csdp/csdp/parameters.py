#	This file is part of csdp-x.
#
#	Copyright (c) 2014 Christian Schmitz <tynn.dev@gmail.com>
#
#	csdp-x is free software: you can redistribute it and/or modify
#	it under the terms of the GNU General Public License as published by
#	the Free Software Foundation, either version 3 of the License, or
#	(at your option) any later version.
#
#	csdp-x is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with csdp-x. If not, see <http://www.gnu.org/licenses/>.

from ctypes import c_double, c_int, Structure

class paramstruc (Structure) :
	_fields_ = [
		("axtol", c_double),
		("atytol", c_double),
		("objtol", c_double),
		("pinftol", c_double),
		("dinftol", c_double),
		("maxiter", c_int),
		("minstepfrac", c_double),
		("maxstepfrac", c_double),
		("minstepp", c_double),
		("minstepd", c_double),
		("usexzgap", c_int),
		("tweakgap", c_int),
		("affine", c_int),
		("perturbobj", c_double),
		("fastmode", c_int),
	]

