#	This file is part of csdp-x.
#
#	Copyright (c) 2014 Christian Schmitz <tynn.dev@gmail.com>
#
#	csdp-x is free software: you can redistribute it and/or modify
#	it under the terms of the GNU General Public License as published by
#	the Free Software Foundation, either version 3 of the License, or
#	(at your option) any later version.
#
#	csdp-x is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with csdp-x. If not, see <http://www.gnu.org/licenses/>.

from ctypes import CFUNCTYPE

from .declarations import _lib

user_exit_callback = CFUNCTYPE(_lib.user_exit.restype, *_lib.user_exit.argtypes)

_lib.register_user_exit_callback.argtypes = [user_exit_callback]
_lib.register_user_exit_callback.restype = None
register_user_exit_callback = _lib.register_user_exit_callback

