#	This file is part of csdp-x.
#
#	Copyright (c) 2014 Christian Schmitz <tynn.dev@gmail.com>
#
#	csdp-x is free software: you can redistribute it and/or modify
#	it under the terms of the GNU General Public License as published by
#	the Free Software Foundation, either version 3 of the License, or
#	(at your option) any later version.
#
#	csdp-x is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with csdp-x. If not, see <http://www.gnu.org/licenses/>.


def solve (input_problem, final_solution = None, initial_solution = None) :
	from . import declarations as csdp

	n = csdp.c_int()
	k = csdp.c_int()
	C = csdp.blockmatrix()
	a = csdp.c_double_p()
	constraints = csdp.constraintmatrix_p()

	if csdp.read_prob(input_problem, n, k, C, a, constraints, 1) :
		raise RuntimeError

	X = csdp.blockmatrix()
	y = csdp.c_double_p()
	Z = csdp.blockmatrix()

	if not initial_solution :
		csdp.initsoln(n, k, C, a, constraints, X, y, Z)
	elif csdp.read_sol(initial_solution, n, k, C, X, y, Z) :
		raise RuntimeError

	pobj = csdp.c_double(0.0)
	dobj = csdp.c_double(0.0)

	ret = csdp.easy_sdp(n, k, C, a, constraints, 0.0, X, y, Z, pobj, dobj)

	if final_solution :
		csdp.write_sol(final_solution, n, k, X, y, Z)

	csdp.free_prob(n, k, C, a, constraints, X, y, Z)

	return ret

if __name__ == '__main__' :
	import sys
	if 2 <= len(sys.argv) <= 4 :
		sys.exit(solve(sys.argv[1:]))
	print ("Usage: python -m csdp.solver "
		"input-problem [final-solution] [initial-solution]")
	sys.exit(1)

