#	This file is part of csdp-x.
#
#	Copyright (c) 2014 Christian Schmitz <tynn.dev@gmail.com>
#
#	csdp-x is free software: you can redistribute it and/or modify
#	it under the terms of the GNU General Public License as published by
#	the Free Software Foundation, either version 3 of the License, or
#	(at your option) any later version.
#
#	csdp-x is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with csdp-x. If not, see <http://www.gnu.org/licenses/>.

from ctypes import c_char_p, CDLL as _CDLL
from ctypes.util import find_library as _find_library
_lib = _CDLL(_find_library("csdp") or "libcsdp.so")

from .index import *
from .blockmat import *
from .parameters import *

c_double_pp = POINTER(c_double_p)
blockmatrix_p = POINTER(blockmatrix)
sparseblock_pp = POINTER(sparseblock_p)
constraintmatrix_pp = POINTER(constraintmatrix_p)
paramstruc_p = POINTER(paramstruc)

_bytes = lambda s : s if isinstance(s, bytes) else s.encode()

_lib.triu.argtypes = [blockmatrix]
_lib.triu.restype = None
triu = _lib.triu

_lib.store_packed.argtypes = [blockmatrix, blockmatrix]
_lib.store_packed.restype = None
store_packed = _lib.store_packed

_lib.store_unpacked.argtypes = [blockmatrix, blockmatrix]
_lib.store_unpacked.restype = None
store_unpacked = _lib.store_unpacked

_lib.alloc_mat_packed.argtypes = [blockmatrix, blockmatrix_p]
_lib.alloc_mat_packed.restype = None
alloc_mat_packed = _lib.alloc_mat_packed

_lib.free_mat_packed.argtypes = [blockmatrix]
_lib.free_mat_packed.restype = None
free_mat_packed = _lib.free_mat_packed

_lib.structnnz.argtypes = [c_int, c_int, blockmatrix, constraintmatrix_p]
structnnz = _lib.structnnz

_lib.actnnz.argtypes = [c_int, c_int, c_double_p]
actnnz = _lib.actnnz

_lib.bandwidth.argtypes = [c_int, c_int, c_double_p]
bandwidth = _lib.bandwidth

_lib.qreig.argtypes = [c_int, c_double_p, c_double_p]
_lib.qreig.restype = None
qreig = _lib.qreig

_lib.sort_entries.argtypes = [c_int, blockmatrix, constraintmatrix_p]
_lib.sort_entries.restype = None
sort_entries = _lib.sort_entries

_lib.norm2.argtypes = [c_int, c_double_p]
_lib.norm2.restype = c_double
norm2 = _lib.norm2

_lib.norm1.argtypes = [c_int, c_double_p]
_lib.norm1.restype = c_double
norm1 = _lib.norm1

_lib.norminf.argtypes = [c_int, c_double_p]
_lib.norminf.restype = c_double
norminf = _lib.norminf

_lib.Fnorm.argtypes = [blockmatrix]
_lib.Fnorm.restype = c_double
Fnorm = _lib.Fnorm

_lib.Knorm.argtypes = [blockmatrix]
_lib.Knorm.restype = c_double
Knorm = _lib.Knorm

_lib.mat1norm.argtypes = [blockmatrix]
_lib.mat1norm.restype = c_double
mat1norm = _lib.mat1norm

_lib.matinfnorm.argtypes = [blockmatrix]
_lib.matinfnorm.restype = c_double
matinfnorm = _lib.matinfnorm

_lib.calc_pobj.argtypes = [blockmatrix, blockmatrix, c_double]
_lib.calc_pobj.restype = c_double
calc_pobj = _lib.calc_pobj

_lib.calc_dobj.argtypes = [c_int, c_double_p, c_double_p, c_double]
_lib.calc_dobj.restype = c_double
calc_dobj = _lib.calc_dobj

_lib.trace_prod.argtypes = [blockmatrix, blockmatrix]
_lib.trace_prod.restype = c_double
trace_prod = _lib.trace_prod

_lib.linesearch.argtypes = [
		c_int, blockmatrix, blockmatrix, blockmatrix, blockmatrix, blockmatrix,
		c_double_p, c_double_p, c_double_p, c_double, c_double, c_int
	]
_lib.linesearch.restype = c_double
linesearch = _lib.linesearch

_lib.pinfeas.argtypes = [
		c_int, constraintmatrix_p, blockmatrix, c_double_p, c_double_p
	]
_lib.pinfeas.restype = c_double
pinfeas = _lib.pinfeas

_lib.dinfeas.argtypes = [
		c_int, blockmatrix, constraintmatrix_p, c_double_p, blockmatrix,
		blockmatrix
	]
_lib.dinfeas.restype = c_double
dinfeas = _lib.dinfeas

_lib.dimacserr3.argtypes = [
		c_int, blockmatrix, constraintmatrix_p, c_double_p, blockmatrix,
		blockmatrix
	]
_lib.dimacserr3.restype = c_double
dimacserr3 = _lib.dimacserr3

_lib.op_a.argtypes = [c_int, constraintmatrix_p, blockmatrix, c_double_p]
_lib.op_a.restype = None
op_a = _lib.op_a

_lib.op_at.argtypes = [c_int, c_double_p, constraintmatrix_p, blockmatrix]
_lib.op_at.restype = None
op_at = _lib.op_at

_lib.makefill.argtypes = [
		c_int, blockmatrix, constraintmatrix_p, constraintmatrix_p, blockmatrix,
		c_int
	]
_lib.makefill.restype = None
makefill = _lib.makefill

_lib.op_o.argtypes = [
		c_int, constraintmatrix_p, sparseblock_pp, blockmatrix, blockmatrix,
		c_double_p, blockmatrix, blockmatrix
	]
_lib.op_o.restype = None
op_o = _lib.op_o

_lib.addscaledmat.argtypes = [blockmatrix, c_double, blockmatrix, blockmatrix]
_lib.addscaledmat.restype = None
addscaledmat = _lib.addscaledmat

_lib.zero_mat.argtypes = [blockmatrix]
_lib.zero_mat.restype = None
zero_mat = _lib.zero_mat

_lib.add_mat.argtypes = [blockmatrix, blockmatrix]
_lib.add_mat.restype = None
add_mat = _lib.add_mat

_lib.sym_mat.argtypes = [blockmatrix]
_lib.sym_mat.restype = None
sym_mat = _lib.sym_mat

_lib.make_i.argtypes = [blockmatrix]
_lib.make_i.restype = None
make_i = _lib.make_i

_lib.copy_mat.argtypes = [blockmatrix, blockmatrix]
_lib.copy_mat.restype = None
copy_mat = _lib.copy_mat

_lib.mat_mult.argtypes = [
		c_double, c_double, blockmatrix, blockmatrix, blockmatrix
	]
_lib.mat_mult.restype = None
mat_mult = _lib.mat_mult

_lib.mat_multspa.argtypes = [
		c_double, c_double, blockmatrix, blockmatrix, blockmatrix,
		constraintmatrix
	]
_lib.mat_multspa.restype = None
mat_multspa = _lib.mat_multspa

_lib.mat_multspb.argtypes = [
		c_double, c_double, blockmatrix, blockmatrix, blockmatrix,
		constraintmatrix
	]
_lib.mat_multspb.restype = None
mat_multspb = _lib.mat_multspb

_lib.mat_multspc.argtypes = [
		c_double, c_double, blockmatrix, blockmatrix, blockmatrix,
		constraintmatrix
	]
_lib.mat_multspc.restype = None
mat_multspc = _lib.mat_multspc

_lib.mat_mult_raw.argtypes = [
		c_int, c_double, c_double, c_double_p, c_double_p, c_double_p
	]
_lib.mat_mult_raw.restype = None
mat_mult_raw = _lib.mat_mult_raw

if hasattr(_lib, "mat_mult_rawatlas") :
	_lib.mat_mult_rawatlas.argtypes = [
			c_int, c_double, c_double, c_double_p, c_double_p, c_double_p
		]
	_lib.mat_mult_rawatlas.restype = None
	mat_mult_rawatlas = _lib.mat_mult_rawatlas

_lib.matvec.argtypes = [blockmatrix, c_double_p, c_double_p]
_lib.matvec.restype = None
matvec = _lib.matvec

_lib.alloc_mat.argtypes = [blockmatrix, blockmatrix_p]
_lib.alloc_mat.restype = None
alloc_mat = _lib.alloc_mat

_lib.free_mat.argtypes = [blockmatrix]
_lib.free_mat.restype = None
free_mat = _lib.free_mat

_lib.initparams.argtypes = [paramstruc_p, c_int_p]
_lib.initparams.restype = None
def initparams (params, pprintlevel = None) :
	if pprintlevel is None : pprintlevel = c_int(0)
	_lib.initparams(params, pprintlevel)

_lib.initsoln.argtypes = [
		c_int, c_int, blockmatrix, c_double_p, constraintmatrix_p,
		blockmatrix_p, c_double_pp, blockmatrix_p
	]
initsoln = _lib.initsoln

_lib.trans.argtypes = [blockmatrix]
_lib.trans.restype = None
trans = _lib.trans

_lib.chol_inv.argtypes = [blockmatrix, blockmatrix]
_lib.chol_inv.restype = None
chol_inv = _lib.chol_inv

_lib.chol.argtypes = [blockmatrix]
chol = _lib.chol

_lib.solvesys.argtypes = [c_int, c_int, c_double_p, c_double_p]
solvesys = _lib.solvesys

_lib.user_exit.argtypes = [
		c_int, c_int, blockmatrix, c_double_p, c_double, c_double, c_double,
		constraintmatrix_p, blockmatrix, c_double_p, blockmatrix, paramstruc
	]
user_exit = _lib.user_exit

_lib.read_sol.argtypes = [
		c_char_p, c_int, c_int, blockmatrix, blockmatrix_p, c_double_pp,
		blockmatrix_p
	]
read_sol = lambda fname, *args : _lib.read_sol(_bytes(fname), *args)

_lib.read_prob.argtypes = [
		c_char_p, c_int_p, c_int_p, blockmatrix_p, c_double_pp,
		constraintmatrix_pp, c_int
	]
read_prob = lambda fname, *args : _lib.read_prob(_bytes(fname), *args)

_lib.write_prob.argtypes = [
		c_char_p, c_int, c_int, blockmatrix, c_double_p, constraintmatrix_p
	]
write_prob = lambda fname, *args : _lib.write_prob(_bytes(fname), *args)

_lib.write_sol.argtypes = [
		c_char_p, c_int, c_int, blockmatrix, c_double_p, blockmatrix
	]
write_sol = lambda fname, *args : _lib.write_sol(_bytes(fname), *args)

_lib.free_prob.argtypes = [
		c_int, c_int, blockmatrix, c_double_p, constraintmatrix_p,
		blockmatrix, c_double_p, blockmatrix
	]
_lib.free_prob.restype = None
free_prob = _lib.free_prob

_lib.sdp.argtypes = [
		c_int, c_int, blockmatrix, c_double_p, c_double, constraintmatrix_p,
		sparseblock_pp, constraintmatrix, blockmatrix, c_double_p,
		blockmatrix, blockmatrix, blockmatrix, c_double_p, c_double_p,
		blockmatrix, blockmatrix, blockmatrix, c_double_p, c_double_p,
		c_double_p, c_double_p, c_double_p, c_double_p, c_double_p,
		c_double_p, c_double_p, blockmatrix, c_double_p, blockmatrix,
		blockmatrix, c_double_p, c_double_p, blockmatrix, blockmatrix,
		c_double_p, c_double_p, c_double_p, c_int, paramstruc
	]
sdp = _lib.sdp

_lib.easy_sdp.argtypes = [
		c_int, c_int, blockmatrix, c_double_p, constraintmatrix_p, c_double,
		blockmatrix_p, c_double_pp, blockmatrix_p, c_double_p, c_double_p
	]
easy_sdp = _lib.easy_sdp

_lib.tweakgap.argtypes = [
		c_int, c_int, c_double_p, constraintmatrix_p, c_double, blockmatrix,
		blockmatrix, c_double_p, c_double_p, blockmatrix, blockmatrix,
		blockmatrix, blockmatrix, c_double_p, c_double_p, c_double_p,
		c_double_p, c_int
	]
_lib.tweakgap.restype = None
tweakgap = _lib.tweakgap

