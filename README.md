csdp-x
======

Some extensions for [CSDP](https://projects.coin-or.org/Csdp).


libcsdp
-------

Static and dynamic CSDP library containing all symbols of libsdp.a.


csdpval
-------

Fix SDPA sparse format problem files for use with CSDP.

CSDP assumes the constraints to be symmetric, but the input
should be an upper triangular matrix.

	Usage: csdpval [+] [edit] input-file

The output is an problem definition in SDPA sparse format,
ordered by rows and columns.  
With the `+` option set, all multiply defined entries of constraints
will be summed up, otherwise the last definition will be kept.  
If `edit` is used, input-file can be a list of files as well.
Each file will be used as its own output-file.


csdp-ld
-------

CSDP binaries dynamically linked against libcsdp.


python-csdp
-----------

Python ctypes wrapper of libcsdp.

