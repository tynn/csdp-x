/*
 *	This file is part of csdp-x.
 *
 *	Copyright (c) 2014 Christian Schmitz <tynn.dev@gmail.com>
 *
 *	csdp-x is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	csdp-x is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with csdp-x. If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include "declarations.h"


#define FREE(var) (free(var), var = NULL)

#define DIGIT "%d"
#define NUMBER "%.18e"
#define SPACE " "
#define NEWLINE "\n"
#define ENTRY DIGIT SPACE DIGIT SPACE DIGIT SPACE DIGIT SPACE NUMBER NEWLINE

#define VALUE(i, j, block) block.data.mat[ijtok(i, j, block.blocksize)]

typedef void (*handle_block)(struct sparseblock *block, int n, FILE *file);


static int handle_file (char *filename, handle_block print_block, bool edit) {
	int n, k, i, j, l;
	struct blockmatrix C;
	double *a;
	struct constraintmatrix *constraints;
	struct sparseblock *block, *next_block;
	FILE *file;

	if (read_prob(filename, &n, &k, &C, &a, &constraints, 0) != 0)
		return 1;

	file = edit ? fopen(filename, "w") : stdout;

	/* number of constraints */
	fprintf(file, DIGIT NEWLINE, k);

	/* number of blocks */
	fprintf(file, DIGIT NEWLINE, C.nblocks);

	/* size of blocks */
	for (n = 1; n <= C.nblocks; n++) {
		if (n > 1)
			fprintf(file, SPACE);
		if (C.blocks[n].blockcategory == DIAG)
			fprintf(file, DIGIT, -C.blocks[n].blocksize);
		else
			fprintf(file, DIGIT, C.blocks[n].blocksize);
	}
	fprintf(file, NEWLINE);

	/* value of a */
	for (i = 1; i <= k; i++) {
		if (i > 1)
			fprintf(file, SPACE);
		fprintf(file, NUMBER, a[i]);
	}
	fprintf(file, NEWLINE);
	FREE(a);

	/* value of C */
	for (n = 1; n <= C.nblocks; n++)
		if (C.blocks[n].blockcategory == DIAG) {
			for (i = 1; i <= C.blocks[n].blocksize; i++)
				if (C.blocks[n].data.vec[i] != 0.0)
					fprintf(file, ENTRY, 0, k, i, i, C.blocks[n].data.vec[i]);

			FREE(C.blocks[n].data.vec);
		} else {
			for (i = 1; i <= C.blocks[n].blocksize; i++)
				for (j = i; j <= C.blocks[n].blocksize; j++)
					if (VALUE(i, j, C.blocks[n]) != 0.0)
						fprintf(file, ENTRY, 0, n, i, j, VALUE(i, j, C.blocks[n]));

			FREE(C.blocks[n].data.mat);
		}
	FREE(C.blocks);

	/* value of constraints */
	for (n = 1; n <= k; n++) {
		block = constraints[n].blocks;

		while (block != NULL)
			if (block->numentries > 0) {
				for (i = 1; i <= block->numentries; i++)
					if (block->iindices[i] > block->jindices[i]) {
						j = block->jindices[i];
						block->jindices[i] = block->iindices[i];
						block->iindices[i] = j;
					}

				for (i = 1; i <= block->numentries; i++) {
					for (j = i + 1, l = i; j <= block->numentries; j++)
						if (block->iindices[j] <= block->iindices[l]
							 && block->jindices[j] <= block->jindices[l])
								l = j;

					block->iindices[i - 1] = block->iindices[l];
					block->jindices[i - 1] = block->jindices[l];
					block->entries[i - 1] =  block->entries[l];

					block->iindices[l] = block->iindices[i];
					block->jindices[l] = block->jindices[i];
					block->entries[l] =  block->entries[i];
				}

				block->iindices[block->numentries] = 0;
				block->jindices[block->numentries] = 0;

				print_block(block, n, file);

				FREE(block->iindices);
				FREE(block->jindices);
				FREE(block->entries);

				next_block = block->next;
				free(block);
				block = next_block;
			}
	}
	FREE(constraints);

	if (file != stdout)
		fclose(file);

	return 0;
}


static void keep_last (struct sparseblock *block, int n, FILE *file) {
	int i;

	for (i = 0; i < block->numentries; i++)
		if (block->iindices[i] != block->iindices[i + 1]
			 || block->jindices[i] != block->jindices[i + 1])
				fprintf(file, ENTRY, n,
					block->blocknum,
					block->iindices[i],
					block->jindices[i],
					block->entries[i]
				);
}


static void sum_all (struct sparseblock *block, int n, FILE *file) {
	int i;
	double e;

	e = 0.0;

	for (i = 0; i < block->numentries; i++)
		if (block->iindices[i] != block->iindices[i + 1]
			 || block->jindices[i] != block->jindices[i + 1]) {
				fprintf(file, ENTRY, n,
					block->blocknum,
					block->iindices[i],
					block->jindices[i],
					e + block->entries[i]
				);
				e = 0.0;
		} else {
			e += block->entries[i];
		}
}


int main (int argc, char **argv) {
	int i;
	handle_block print_block;

	if (argc == 2)
		return handle_file(argv[1], &keep_last, false);

	if (argc > 2) {
		if (strcmp(argv[1], "+")) {
			i = 1;
			print_block = &keep_last;
		} else if (argc == 3) {
			return handle_file(argv[2], &sum_all, false);
		} else {
			i = 2;
			print_block = &sum_all;
		}

		if (strcmp(argv[i], "edit") == 0) {
			for (i = i + 1; i < argc; i++)
				handle_file(argv[i], print_block, true);

			return 0;
		}
	}

	fprintf(stderr, "Usage: %s [+] [edit] input-file \n", argv[0]);
	return 1;
}

