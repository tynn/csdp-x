/*
 *	This file is part of csdp-x.
 *
 *	Copyright (c) 2014 Christian Schmitz <tynn.dev@gmail.com>
 *
 *	csdp-x is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	csdp-x is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with csdp-x. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __x_user_exit_h__
#define __x_user_exit_h__

#include "declarations.h"

#define ARGTYPES int n, int k, struct blockmatrix C, double *a, double dobj, \
	double pobj, double constant_offset, struct constraintmatrix *constraints, \
	struct blockmatrix X, double *y, struct blockmatrix Z, struct paramstruc params

typedef int (*user_exit_callback) (ARGTYPES);

void register_user_exit_callback (user_exit_callback callback);

#endif // __x_user_exit_h__

