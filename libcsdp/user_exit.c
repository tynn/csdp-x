/*
 *	This file is part of csdp-x.
 *
 *	Copyright (c) 2014 Christian Schmitz <tynn.dev@gmail.com>
 *
 *	csdp-x is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	csdp-x is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with csdp-x. If not, see <http://www.gnu.org/licenses/>.
 */

#include "user_exit.h"

static user_exit_callback _c;

void register_user_exit_callback (user_exit_callback callback) {
	_c = callback;
}

int user_exit (ARGTYPES) {
	if (!_c) return 0;
	return _c(n, k, C, a, dobj, pobj, constant_offset, constraints, X, y, Z, params);
}

